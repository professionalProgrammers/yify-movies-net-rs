use crate::{
    MoviePage,
    SearchPage,
    YifyError,
    YifyResult,
};
use select::document::Document;

/// A client
#[derive(Debug, Clone)]
pub struct Client {
    client: reqwest::Client,
}

impl Client {
    /// Make a new client
    pub fn new() -> Self {
        Client {
            client: reqwest::Client::new(),
        }
    }

    /// Search with a query. `page` starts at 1.
    pub async fn search(&self, query: &str, page: usize) -> YifyResult<SearchPage> {
        let url = format!(
            "https://www.yify-movies.net/search/{query}/time/{page}/",
            query = query,
            page = page
        );

        let res = self.client.get(&url).send().await?;
        let status = res.status();

        if !status.is_success() {
            return Err(YifyError::InvalidStatus(status));
        }

        let text = res.text().await?;
        let doc = Document::from(text.as_str());

        let page = SearchPage::from_doc(&doc).ok_or(YifyError::InvalidSearchPage)?;

        Ok(page)
    }

    /// Get a movie page
    pub async fn get_movie(&self, url: &str) -> YifyResult<MoviePage> {
        let res = self.client.get(url).send().await?;
        let status = res.status();

        if !status.is_success() {
            return Err(YifyError::InvalidStatus(status));
        }

        let text = res.text().await?;
        let doc = Document::from(text.as_str());
        let page = MoviePage::from_doc(&doc).ok_or(YifyError::InvalidSearchPage)?;

        Ok(page)
    }
}

impl Default for Client {
    fn default() -> Self {
        Client::new()
    }
}
