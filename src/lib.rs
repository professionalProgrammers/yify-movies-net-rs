mod client;
pub mod types;

pub use crate::{
    client::Client,
    types::{
        MoviePage,
        SearchEntry,
        SearchPage,
    },
};

/// the result type of this lib
pub type YifyResult<T> = Result<T, YifyError>;

/// The error type of this lib
#[derive(Debug, thiserror::Error)]
pub enum YifyError {
    #[error("{0}")]
    Reqwest(#[from] reqwest::Error),

    #[error("invalid status: {0}")]
    InvalidStatus(reqwest::StatusCode),

    #[error("the search page is invalid")]
    InvalidSearchPage,
}

#[cfg(test)]
mod tests {
    use super::*;

    #[tokio::test]
    async fn search() {
        let client = Client::new();

        let _result = client.search("star", 1).await.unwrap();
    }

    #[tokio::test]
    async fn get_movie() {
        let client = Client::new();

        let _result = client
            .get_movie("https://www.yify-movies.net/movies/mulan-2020-yify-1080p.html")
            .await
            .unwrap();
    }
}
