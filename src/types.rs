use reqwest::Url;
use select::{
    document::Document,
    node::Node,
    predicate::{
        Class,
        Name,
        Text,
    },
};

/// The response to a search
#[derive(Debug)]
pub struct SearchPage {
    /// The search query
    pub query: String,

    /// A collection of search results
    pub entries: Vec<SearchEntry>,

    /// The total number of results
    pub total_results: usize,
}

impl SearchPage {
    /// Try to create this from a doc
    pub(crate) fn from_doc(doc: &Document) -> Option<Self> {
        let main_div = doc.find(Class("main")).next()?;
        let section_div = main_div.find(Class("section")).next()?;
        let heading_div = main_div.find(Class("heading")).next()?;

        let heading_content = heading_div
            .find(Name("h3"))
            .next()?
            .find(Text)
            .next()?
            .as_text()?
            .trim();
        let mut heading_content_iter = heading_content.split(' ');

        if heading_content_iter.next()? != "Search" {
            return None;
        }

        let total_results = heading_content_iter.next()?.parse::<usize>().ok()?;

        if heading_content_iter.next()? != "Result(s)" {
            return None;
        }

        if heading_content_iter.next()? != "for" {
            return None;
        }

        let query = heading_content_iter.next()?.to_string();

        let entries = section_div
            .find(Class("grid"))
            .map(SearchEntry::from_node)
            .collect::<Option<Vec<SearchEntry>>>()?;

        Some(Self {
            query,
            entries,
            total_results,
        })
    }
}

/// An entry in a search result
#[derive(Debug)]
pub struct SearchEntry {
    /// Movie title
    pub title: String,

    pub img: Url,

    /// The link to a resource
    pub link: Url,

    /// The quality of this movie
    pub quality: String,
}

impl SearchEntry {
    /// Try to create a search entry from a select node predeterimned to be a search entry
    pub(crate) fn from_node(n: Node) -> Option<Self> {
        let link_el = n.find(Name("a")).next()?;
        let link = {
            let link = Url::parse("https://www.yify-movies.net").ok()?;
            link.join(link_el.attr("href")?).ok()?
        };

        let img = Url::parse(link_el.find(Name("img")).next()?.attr("src")?).ok()?;

        let title = n
            .find(Name("h2"))
            .next()?
            .find(Text)
            .next()?
            .as_text()?
            .trim()
            .to_string();

        let quality = n
            .find(Class("movie-details"))
            .next()?
            .find(Class("rupees"))
            .next()?
            .find(Text)
            .next()?
            .as_text()?
            .trim()
            .to_string();

        Some(SearchEntry {
            title,
            img,
            link,
            quality,
        })
    }
}

/// A movie page
#[derive(Debug)]
pub struct MoviePage {
    /// The movie title
    pub title: String,

    /// The movie description
    pub description: String,

    /// The movie genres
    pub genre: Vec<String>,

    /// The movie quality
    pub quality: String,

    /// The movie size
    pub size: String,

    /// The movie resolution
    pub resolution: String,

    /// The movie language
    pub language: String,

    /// The movie run time
    pub run_time: String,

    /// The movie's imdb rating
    pub imdb_rating: String,

    /// The movie mpr
    pub mpr: String,

    /// The movie peers/seeds
    pub peers_seeds: String,

    /// The download link
    pub download_link: String,
}

impl MoviePage {
    /// Try to create a movie page from a doc
    pub(crate) fn from_doc(doc: &Document) -> Option<Self> {
        let main_div = doc.find(Class("main")).next()?;
        let section_div = main_div.find(Class("section")).next()?;
        let desc_div = section_div.find(Class("desc")).next()?;
        let wishlist_div = desc_div.find(Class("wish-list")).next()?;

        let mut quality = None;
        let mut genre = None;
        let mut size = None;
        let mut resolution = None;
        let mut language = None;
        let mut run_time = None;
        let mut imdb_rating = None;
        let mut mpr = None;
        let mut peers_seeds = None;
        for el in desc_div
            .find(Class("available"))
            .next()?
            .find(Name("ul"))
            .next()?
            .find(Name("li"))
        {
            let key = el.find(Name("b")).next()?.find(Text).next()?.as_text()?;

            match key {
                "Genre:" => {
                    genre = Some(
                        el.find(Name("a"))
                            .map(|el| Some(el.find(Text).next()?.as_text()?.trim().to_string()))
                            .collect::<Option<Vec<String>>>()?,
                    );
                }
                "Quality:" => {
                    quality = Some(
                        el.children()
                            .find_map(|el| el.as_text())?
                            .trim()
                            .to_string(),
                    );
                }
                "Size:" => {
                    size = Some(
                        el.children()
                            .find_map(|el| el.as_text())?
                            .trim()
                            .to_string(),
                    );
                }
                "Resolution:" => {
                    resolution = Some(
                        el.children()
                            .find_map(|el| el.as_text())?
                            .trim()
                            .to_string(),
                    );
                }
                "Language:" => {
                    language = Some(
                        el.children()
                            .find_map(|el| el.as_text())?
                            .trim()
                            .to_string(),
                    );
                }
                "Run Time:" => {
                    run_time = Some(
                        el.children()
                            .find_map(|el| el.as_text())?
                            .trim()
                            .to_string(),
                    );
                }
                "IMDB Rating:" => {
                    imdb_rating = Some(
                        el.children()
                            .find_map(|el| el.as_text())?
                            .trim()
                            .to_string(),
                    );
                }
                "MPR:" => {
                    mpr = Some(
                        el.children()
                            .find_map(|el| el.as_text())?
                            .trim()
                            .to_string(),
                    );
                }
                "Peers/Seeds:" => {
                    peers_seeds = Some(
                        el.children()
                            .find_map(|el| el.as_text())?
                            .trim()
                            .to_string(),
                    );
                }
                _ => {}
            }
        }
        let quality = quality?;
        let genre = genre?;
        let size = size?;
        let resolution = resolution?;
        let language = language?;
        let run_time = run_time?;
        let imdb_rating = imdb_rating?;
        let mpr = mpr?;
        let peers_seeds = peers_seeds?;

        let title = desc_div
            .find(Name("h2"))
            .next()?
            .find(Text)
            .next()?
            .as_text()?
            .trim()
            .trim_end_matches(&quality)
            .trim()
            .to_string();

        let description = desc_div
            .find(Name("p"))
            .next()?
            .find(Text)
            .next()?
            .as_text()?
            .trim()
            .to_string();

        let download_link = wishlist_div
            .find(Class("wish"))
            .next()?
            .find(Name("a"))
            .next()?
            .attr("href")?
            .to_string();

        Some(MoviePage {
            title,
            description,
            genre,
            quality,
            size,
            resolution,
            language,
            run_time,
            imdb_rating,
            mpr,
            peers_seeds,
            download_link,
        })
    }
}

#[cfg(test)]
mod test {
    use super::*;

    const OLD_SEARCH_PAGE: &str = include_str!("../test_data/old_search_page.html");

    const MOVIE_PAGE: &str = include_str!("../test_data/movie_page.html");

    #[test]
    fn parse_search_page() {
        let doc = Document::from(OLD_SEARCH_PAGE);

        let page = SearchPage::from_doc(&doc).unwrap();
        assert!(!page.entries.is_empty());
    }

    #[test]
    fn parse_movie_page() {
        let doc = Document::from(MOVIE_PAGE);

        let _page = MoviePage::from_doc(&doc).unwrap();
    }
}
